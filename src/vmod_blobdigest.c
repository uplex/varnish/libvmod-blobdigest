/*-
 * Copyright 2016 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "cache/cache.h"
#include "vcl.h"
#include "vas.h"
#include "vsb.h"
#include "vtim.h"

#include "vcc_blobdigest_if.h"
#include "vmod_blobdigest.h"

#include "byte_order.h"

#define ERR(fail, ctx, msg) do {					\
		if (fail)						\
			VRT_fail((ctx), "vmod blobdigest error: " msg);	\
		else							\
			VSLbs((ctx)->vsl, SLT_Error, TOSTRAND(msg));	\
	} while (0)

#define VERR(fail, ctx, fmt, ...) do {					\
		if (fail)						\
			VRT_fail((ctx), "vmod blobdigest error: " fmt,	\
				 __VA_ARGS__);				\
		else							\
			VSLb((ctx)->vsl, SLT_Error,			\
			     "vmod blobdigest error: " fmt, __VA_ARGS__); \
	} while (0)

#define VERRNOMEM(fail, ctx, fmt, ...)				\
	VERR(fail, (ctx), fmt ", out of space", __VA_ARGS__)

#define ERRNOMEM(fail, ctx, msg)		\
	ERR(fail, (ctx), msg ", out of space")

#define FAIL(ctx, msg)				\
	ERR(1, ctx, msg)

#define VFAIL(ctx, fmt, ...)			\
	VERR(1, ctx, fmt, __VA_ARGS__)

#define VFAILNOMEM(ctx, fmt, ...)		\
	VERRNOMEM(1, ctx, fmt, __VA_ARGS__)

#define FAILNOMEM(ctx, msg)			\
	ERRNOMEM(1, ctx, msg)

#define INIT_FINI(ctx) (((ctx)->method & (VCL_MET_INIT | VCL_MET_FINI)) != 0)

#define BLOB_BLOBDIGEST_DIGEST_TYPE 0x9235d52f

struct digest_task {
	unsigned	magic;
#define VMOD_BLOBDIGEST_DIGEST_TASK_MAGIC 0x646937a8
	hash_ctx	ctx;
	VCL_BLOB	result;
};

struct vmod_blobdigest_digest {
	unsigned	magic;
#define VMOD_BLOBDIGEST_DIGEST_MAGIC 0xaccb2e25
	hash_ctx	ctx;
	char		*vcl_name;
	VCL_BLOB	result;
	void		*digest;	// == result->blob
	enum algorithm	hash;
	enum scope	scope;
};

struct vmod_blobdigest_hmac {
	unsigned	magic;
#define VMOD_BLOBDIGEST_HMAC_MAGIC 0x85678153
	hash_ctx	inner_ctx;
	hash_ctx	outer_ctx;
	char		*vcl_name;
	enum algorithm	hash;
};

static enum algorithm
parse_algorithm(VCL_ENUM e)
{
#define VMODENUM(n) if (e == VENUM(n)) return(n);
#include "tbl_algorithm.h"
	WRONG("illegal algorithm enum");
}

static enum scope
parse_scope(VCL_ENUM e)
{
#define VMODENUM(n) if (e == VENUM(n)) return(n);
#include "tbl_scope.h"
	WRONG("illegal scope enum");
}

/* too simple to qualify for an own source file */
static void
hash_rs(uint32_t state[2], const unsigned char *msg, size_t size)
{
	/* hash function from Robert Sedgwicks 'Algorithms in C' book */
	uint32_t res		= state[0];
	uint32_t a		= state[1];
	const uint32_t b	= 378551;
	const unsigned char *e;

	for (e = msg + size; msg < e; msg++) {
		res = res * a + *msg;
		a *= b;
	}

	state[0] = res;
	state[1] = a;
}

static void
init(const enum algorithm hash, hash_ctx * const hctx)
{
	switch(hash) {
	case ICRC32:
		hctx->uint32[0] = ~0U;
		break;
	case CRC32:
		hctx->uint32[0] = 0;
		break;
	case MD5:
		rhash_md5_init(&hctx->md5);
		break;
	case RS:
		hctx->uint32[0] = 0;
		hctx->uint32[1] = 63689;
		break;
	case SHA1:
		rhash_sha1_init(&hctx->sha1);
		break;
	case SHA224:
		rhash_sha224_init(&hctx->sha224);
		break;
	case SHA256:
		SHA256_Init(&hctx->sha256);
		break;
	case SHA384:
		rhash_sha384_init(&hctx->sha512);
		break;
	case SHA512:
		rhash_sha512_init(&hctx->sha512);
		break;
	case SHA3_224:
		rhash_sha3_224_init(&hctx->sha3);
		break;
	case SHA3_256:
		rhash_sha3_256_init(&hctx->sha3);
		break;
	case SHA3_384:
		rhash_sha3_384_init(&hctx->sha3);
		break;
	case SHA3_512:
		rhash_sha3_512_init(&hctx->sha3);
		break;
	default:
		WRONG("illegal algorithm");
	}
}

static void
update(const enum algorithm hash, hash_ctx *restrict const hctx,
       const uint8_t *restrict const msg, const size_t len)
{
	switch(hash) {
	case ICRC32:
	case CRC32:
		hctx->uint32[0] = rhash_get_crc32(hctx->uint32[0], msg, len);
		break;
	case MD5:
		rhash_md5_update(&hctx->md5, msg, len);
		break;
	case RS:
		hash_rs(hctx->uint32, msg, len);
		break;
	case SHA1:
		rhash_sha1_update(&hctx->sha1, msg, len);
		break;
	case SHA224:
		rhash_sha256_update(&hctx->sha224, msg, len);
		break;
	case SHA256:
		SHA256_Update(&hctx->sha256, msg, len);
		break;
	case SHA384:
	case SHA512:
		rhash_sha512_update(&hctx->sha512, msg, len);
		break;
	case SHA3_224:
	case SHA3_256:
	case SHA3_384:
	case SHA3_512:
		rhash_sha3_update(&hctx->sha3, msg, len);
		break;
	default:
		WRONG("illegal algorithm");
	}
}

struct blobdigest_iter_priv {
	unsigned			magic;
#define BLOBDIGEST_ITER_PRIV_MAGIC	0x7b7dbbaf
	enum algorithm			hash;
	hash_ctx *restrict const	hctx;
};

static int v_matchproto_(objiterate_f)
blobdigest_iter_f(void *priv, unsigned flush, const void *ptr, ssize_t len)
{
	struct blobdigest_iter_priv	*bi;

	CAST_OBJ_NOTNULL(bi, priv, BLOBDIGEST_ITER_PRIV_MAGIC);
	(void) flush;

	update(bi->hash, bi->hctx, ptr, len);
	return (0);
}

static const char *
blobdigest_vrb_iter(struct worker *wrk, struct vsl_log *vsl, struct req *req,
		    struct blobdigest_iter_priv *bi)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	if (req->req_body_status == BS_NONE)
		return (NULL);
	if (req->req_body_status == BS_ERROR)
		return ("previous error on body (BS_ERROR)");
	if (req->req_body_status == BS_TAKEN)
		return ("body already taken (BS_TAKEN)");

	(void) VRB_Iterate(wrk, vsl, req, blobdigest_iter_f, bi);
	return (NULL);
}


static const char *
update_body(VRT_CTX, VCL_ENUM from,
	    const enum algorithm hash, hash_ctx *restrict const hctx)
{
	struct blobdigest_iter_priv bi[1] = {{
		.magic = BLOBDIGEST_ITER_PRIV_MAGIC,
		.hash = hash,
		.hctx = hctx
		}};
	struct req *req = ctx->req;

	if (from == VENUM(req_body)) {
		if (req == NULL)
			return ("from = req_body, "
				"but no request body found");
		return (blobdigest_vrb_iter(req->wrk, ctx->vsl, req, bi));
	}
	else if (from == VENUM(bereq_body) &&
	     ctx->bo != NULL && ctx->bo->bereq_body != NULL) {
		(void) ObjIterate(ctx->bo->wrk, ctx->bo->bereq_body,
				  bi, blobdigest_iter_f, 0);
	}
	else if (from == VENUM(bereq_body)) {
		if (ctx->bo == NULL)
			return ("from = bereq_body, "
				"but no backend request body found");
		req = ctx->bo->req;
		/* no req == no body */
		if (req == NULL)
			return (NULL);
		return (blobdigest_vrb_iter(req->wrk, ctx->vsl, req, bi));
	}
	else if (from == VENUM(resp_body)) {
		if (req == NULL || req->objcore == NULL)
			return ("from = resp_body, "
				"but no response body found");
		(void) ObjIterate(req->wrk, req->objcore,
				  bi, blobdigest_iter_f, 0);
	}
	else
		WRONG("from VENUM");
	return (NULL);
}


static void
final(const enum algorithm hash, hash_ctx *restrict const hctx,
      uint8_t *restrict result)
{
	switch(hash) {
	case ICRC32:
		hctx->uint32[0] ^= ~0U;
		/* FALLTHROUGH */
	case CRC32:
	case RS:
		be32_copy(result, 0, &hctx->uint32[0], sizeof(uint32_t));
		break;
	case MD5:
		rhash_md5_final(&hctx->md5, result);
		break;
	case SHA1:
		rhash_sha1_final(&hctx->sha1, result);
		break;
	case SHA224:
		rhash_sha256_final(&hctx->sha224, result);
		break;
	case SHA256:
		SHA256_Final(result, &hctx->sha256);
		break;
	case SHA384:
	case SHA512:
		rhash_sha512_final(&hctx->sha512, result);
		break;
	case SHA3_224:
	case SHA3_256:
	case SHA3_384:
	case SHA3_512:
		rhash_sha3_final(&hctx->sha3, result);
		break;
	default:
		WRONG("illegal algorithm");
	}
}

static inline void
digest(const enum algorithm hash, hash_ctx *restrict const hctx,
       VCL_BLOB restrict const b, uint8_t *restrict digest)
{
	init(hash, hctx);
	update(hash, hctx, b->blob, b->len);
	final(hash, hctx, digest);
}

static VCL_BLOB
ws_alloc_digest(VRT_CTX, const size_t digestsz, void **digestp,
		const char * const restrict context,
		const char * const restrict caller)
{
	unsigned char *spc;
	struct vrt_blob *b;

	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	AN(digestp);

	spc = WS_Alloc(ctx->ws, PRNDUP(digestsz) + sizeof *b);
	if (spc == NULL) {
		VFAILNOMEM(ctx, "WS_Alloc in %s.%s()", context, caller);
		return (NULL);
	}

	b = (void *)(spc + PRNDUP(digestsz));
	b->blob = *digestp = spc;
	b->type = BLOB_BLOBDIGEST_DIGEST_TYPE;
	b->len = digestsz;

	return (b);
}

static VCL_BLOB
heap_alloc_digest(VRT_CTX, const size_t digestsz, void **digestp,
		const char * const restrict context,
		const char * const restrict caller)
{
	unsigned char *spc;
	struct vrt_blob *b;

	AN(digestp);

	spc = malloc(PRNDUP(digestsz) + sizeof *b);
	if (spc == NULL) {
		VFAILNOMEM(ctx, "malloc in %s.%s()", context, caller);
		return (NULL);
	}

	b = (void *)(spc + PRNDUP(digestsz));
	b->blob = *digestp = spc;
	b->type = BLOB_BLOBDIGEST_DIGEST_TYPE;
	b->len = digestsz;

	return (b);
}

/* Objects */

static inline void
WS_Contains(struct ws * const restrict ws, const void * const restrict ptr,
	    const size_t len)
{
	assert((char *)ptr >= ws->s && (char *)(ptr + len) <= ws->e);
}

static struct digest_task *
get_scope(const struct vrt_ctx * const restrict ctx,
	  const struct vmod_blobdigest_digest * const restrict h,
	  const char * const restrict method)
{
	struct vmod_priv *priv;
	struct digest_task *task;
	struct ws *ws;

	switch (h->scope) {
	case TASK:
		priv = VRT_priv_task(ctx, (void *)h);
		ws = ctx->ws;
		break;
	case TOP: {
		if (ctx->req == NULL) {
			VFAIL(ctx, "%s.%s(): object has TOP scope - only "
			    "accessible in client VCL context",
			    h->vcl_name, method);
			return (NULL);
		}

		priv = VRT_priv_top(ctx, (void *)h);
		if (ctx->req->top)
			ws = ctx->req->top->topreq->ws;
		else
			ws = ctx->req->ws;
		break;
	}
	default:
		WRONG("invalid scope");
	}

	if (priv == NULL) {
		FAIL(ctx, "no priv - out of workspace?");
		return (NULL);
	}

	AN(priv);
	AN(ws);

	if (priv->priv != NULL) {
		CAST_OBJ(task, priv->priv, VMOD_BLOBDIGEST_DIGEST_TASK_MAGIC);
		WS_Contains(ws, task, sizeof(struct digest_task));
	}
	else {
		task = WS_Alloc(ws, sizeof(struct digest_task));
		if (task == NULL) {
			VFAILNOMEM(ctx, "allocating task data in %s.%s()",
			    h->vcl_name, method);
			return (NULL);
		}
		task->magic = VMOD_BLOBDIGEST_DIGEST_TASK_MAGIC;
		memcpy(&task->ctx, &h->ctx, sizeof(hash_ctx));
		task->result = NULL;
		priv->priv = task;
		priv->len = sizeof(struct digest_task);
		AZ(priv->methods);
	}
	return (task);
}

VCL_VOID
vmod_digest__init(VRT_CTX, struct vmod_blobdigest_digest **digestp,
		  const char *vcl_name, VCL_ENUM hashs, VCL_BLOB initb,
		  VCL_ENUM scopes)
{
	struct vmod_blobdigest_digest *digest;
	enum algorithm hash = parse_algorithm(hashs);
	enum scope scope = parse_scope(scopes);

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(digestp);
	AZ(*digestp);
	AN(vcl_name);
	ALLOC_OBJ(digest, VMOD_BLOBDIGEST_DIGEST_MAGIC);
	AN(digest);
	*digestp = digest;

	digest->scope = scope;
	digest->hash = hash;
	digest->vcl_name = strdup(vcl_name);
	AN(digest->vcl_name);
	AZ(digest->result);
	init(hash, &digest->ctx);
	if (initb != NULL && initb->len > 0 && initb->blob != NULL)
		update(hash, &digest->ctx, initb->blob, initb->len);
}

VCL_VOID
vmod_digest__fini(struct vmod_blobdigest_digest **digestp)
{
	struct vmod_blobdigest_digest *digest;

	digest = *digestp;
	if (digest == NULL)
		return;

	*digestp = NULL;
	CHECK_OBJ_NOTNULL(digest, VMOD_BLOBDIGEST_DIGEST_MAGIC);
	/* single allocation for digest + blob */
	if (digest->digest != NULL)
		free(digest->digest);
	if (digest->vcl_name != NULL)
		free(digest->vcl_name);
	FREE_OBJ(digest);
}

VCL_BOOL
vmod_digest_update(VRT_CTX, struct vmod_blobdigest_digest *h,
		   struct VARGS(digest_update) *a)
{
	struct digest_task *task;
	hash_ctx *hctx;
	const char *err = NULL;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(a);
	CHECK_OBJ_NOTNULL(h, VMOD_BLOBDIGEST_DIGEST_MAGIC);

	if (h->result != NULL) {
		VERR(a->fail, ctx, "already finalized in %s.update()",
		     h->vcl_name);
		return (0);
	}

	task = get_scope(ctx, h, "update");
	if (task == NULL)
		return (0);

	if (a->valid_msg && a->msg == NULL) {
		VERR(a->fail, ctx, "null BLOB passed to %s.update()",
		     h->vcl_name);
		return (0);
	}

	hctx = INIT_FINI(ctx) ? &h->ctx : &task->ctx;

	if (task->result != NULL) {
		VERR(a->fail, ctx, "already finalized in %s.update()",
		     h->vcl_name);
		return (0);
	}
	if (a->valid_msg && a->msg->len > 0 && a->msg->blob != NULL)
		update(h->hash, hctx, a->msg->blob, a->msg->len);
	if (a->valid_from)
		err = update_body(ctx, a->from, h->hash, hctx);
	if (err != NULL) {
		VERR(a->fail, ctx, "%s in %s.update()", err, h->vcl_name);
		return (0);
	}
	return (1);
}

VCL_BLOB
vmod_digest_final(VRT_CTX, struct vmod_blobdigest_digest *h)
{
	VCL_BLOB b;
	void *r = NULL;
	struct digest_task *task;
	hash_ctx *hctx;
	enum algorithm hash;
	size_t digestsz;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(h, VMOD_BLOBDIGEST_DIGEST_MAGIC);

	if (h->result != NULL)
		return (h->result);

	hash = h->hash;
	digestsz = hashspec[hash].digestsz;

	if (INIT_FINI(ctx)) {
		b = heap_alloc_digest(ctx, digestsz, &r, h->vcl_name, "final");
		h->result = b;
		hctx = &h->ctx;
	} else {
		task = get_scope(ctx, h, "final");
		if (task == NULL)
			return (NULL);

		if (task->result != NULL)
			return (task->result);

		b = ws_alloc_digest(ctx, digestsz, &r, h->vcl_name, "final");
		task->result = b;
		hctx = &task->ctx;
	}
	if (b == NULL)
		return (NULL);
	AN(r);
	final(hash, hctx, r);
	return (b);
}

static void
hmac_init(enum algorithm hash, VCL_BLOB restrict key,
	  hash_ctx * const restrict inner_ctx,
	  hash_ctx * const restrict outer_ctx)
{
	size_t blocksz = hashspec[hash].blocksz;
	uint8_t k[blocksz], innerk[blocksz], outerk[blocksz];

	memset(k, 0, blocksz);
	if (key->len <= blocksz)
		memcpy(k, key->blob, key->len);
	else {
		hash_ctx hctx[1];

		assert(blocksz >= hashspec[hash].digestsz);
		digest(hash, hctx, key, k);
	}
	for (int i = 0; i < blocksz; i++) {
		innerk[i] = k[i] ^ 0x36;
		outerk[i] = k[i] ^ 0x5c;
	}
	init(hash, inner_ctx);
	init(hash, outer_ctx);
	update(hash, inner_ctx, innerk, blocksz);
	update(hash, outer_ctx, outerk, blocksz);
}

static VCL_BLOB
hmac_final(VRT_CTX, enum algorithm hash, VCL_BLOB restrict msg,
	   hash_ctx * const restrict inner_ctx,
	   hash_ctx * const restrict outer_ctx,
	   const char * const restrict context,
	   const char * const restrict caller)
{
	void *digest = NULL;
	VCL_BLOB b;
	size_t digestsz = hashspec[hash].digestsz;

	b = ws_alloc_digest(ctx, digestsz, &digest, context, caller);
	if (b == NULL)
		return (NULL);

	AN(digest);

	uint8_t inner_digest[digestsz];

	/* Hash the message with the inner key */
	update(hash, inner_ctx, msg->blob, msg->len);
	final(hash, inner_ctx, inner_digest);

	/* Hash the result with the outer key */
	update(hash, outer_ctx, inner_digest, digestsz);
	final(hash, outer_ctx, digest);
	return (b);
}

VCL_VOID
vmod_hmac__init(VRT_CTX, struct vmod_blobdigest_hmac **hmacp,
		const char *vcl_name, VCL_ENUM hashs, VCL_BLOB key)
{
	struct vmod_blobdigest_hmac *hmac;
	enum algorithm hash = parse_algorithm(hashs);

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(vcl_name);
	if (key == NULL || key->blob == NULL) {
		VFAIL(ctx, "key is NULL in %s constructor", vcl_name);
		return;
	}
	AN(hmacp);
	AZ(*hmacp);
	ALLOC_OBJ(hmac, VMOD_BLOBDIGEST_HMAC_MAGIC);
	AN(hmac);

	*hmacp = hmac;
	hmac->hash = hash;
	hmac->vcl_name = strdup(vcl_name);
	hmac_init(hash, key, &hmac->inner_ctx, &hmac->outer_ctx);
}

VCL_BLOB
vmod_hmac_hmac(VRT_CTX, struct vmod_blobdigest_hmac *h, VCL_BLOB msg)
{
	hash_ctx inner_ctx, outer_ctx;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(h, VMOD_BLOBDIGEST_HMAC_MAGIC);
	if (msg == NULL || msg->blob == NULL) {
		VFAIL(ctx, "msg is NULL in %s.hmac()", h->vcl_name);
		return (NULL);
	}

	memcpy(&inner_ctx, &h->inner_ctx, sizeof(hash_ctx));
	memcpy(&outer_ctx, &h->outer_ctx, sizeof(hash_ctx));

	return (hmac_final(ctx, h->hash, msg, &inner_ctx, &outer_ctx,
	    h->vcl_name, "hmac"));
}

VCL_DURATION
vmod_hmac_hmac_bench(VRT_CTX, struct vmod_blobdigest_hmac *h, VCL_INT n,
		     VCL_BLOB msg)
{
	double t0, t1;
	uintptr_t snap;

	if (n <= 0) {
		FAIL(ctx, "number of rounds must be greater than zero");
		return (-1);
	}
	snap = WS_Snapshot(ctx->ws);
	t0 = VTIM_mono();
	while (n--) {
		WS_Reset(ctx->ws, snap);
		(void) vmod_hmac_hmac(ctx, h, msg);
	}
	t1 = VTIM_mono();
	return (t1 - t0);
}

VCL_VOID
vmod_hmac__fini(struct vmod_blobdigest_hmac **hmacp)
{
	struct vmod_blobdigest_hmac *hmac;

	if (*hmacp == NULL)
		return;
	hmac = *hmacp;
	*hmacp = NULL;
	CHECK_OBJ_NOTNULL(hmac, VMOD_BLOBDIGEST_HMAC_MAGIC);
	if (hmac->vcl_name != NULL)
		free(hmac->vcl_name);
	FREE_OBJ(hmac);
}

/* Functions */

VCL_BLOB
vmod_hash(VRT_CTX, VCL_ENUM hashs, VCL_BLOB msg)
{
	enum algorithm hash = parse_algorithm(hashs);
	void *r = NULL;
	VCL_BLOB b;
	hash_ctx hctx[1];

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	if (msg == NULL)
		return (NULL);

	b = ws_alloc_digest(ctx, hashspec[hash].digestsz, &r,
	    "blobdigest", "hash");

	if (b == NULL)
		return (NULL);

	AN(r);
	digest(hash, hctx, msg, r);

	return (b);
}

VCL_BLOB
vmod_hmacf(VRT_CTX, VCL_ENUM hashs, VCL_BLOB key, VCL_BLOB msg)
{
	enum algorithm hash = parse_algorithm(hashs);
	hash_ctx inner_ctx, outer_ctx;

	if (key == NULL || key->blob == NULL) {
		FAIL(ctx, "key is NULL in blobdigest.hmacf()");
		return (NULL);
	}
	if (msg == NULL || msg->blob == NULL) {
		FAIL(ctx, "msg is NULL in blobdigest.hmacf()");
		return (NULL);
	}
	hmac_init(hash, key, &inner_ctx, &outer_ctx);
	return (hmac_final(ctx, hash, msg, &inner_ctx, &outer_ctx, "blobdigest",
	    "hmacf"));
}

VCL_STRING
vmod_version(VRT_CTX __attribute__((unused)))
{
	return (VERSION);
}

/* vend.h is varinshd private */
static __inline void
vbe32enc(void *pp, uint32_t u)
{
	uint8_t *p = (uint8_t *)pp;

	p[0] = (u >> 24) & 0xff;
	p[1] = (u >> 16) & 0xff;
	p[2] = (u >> 8) & 0xff;
	p[3] = u & 0xff;
}
