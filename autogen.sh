#!/bin/sh

set -e
set -u

if ! command -v libtoolize >/dev/null
then
	echo "libtoolize: command not found, falling back to glibtoolize" >&2
	alias libtoolize=glibtoolize
fi

mkdir -p m4
aclocal
libtoolize --copy --force
autoheader
automake --add-missing --copy --foreign
autoconf
