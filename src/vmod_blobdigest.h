/*-
 * Copyright 2016 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <sys/types.h>

#include "crc32.h"
#include "md5.h"
#include "sha1.h"
#include "vsha256.h"
#include "sha256.h"
#include "sha512.h"
#include "sha3.h"

#define SHA3_BLOCKSZ(bits) ((1600 - (bits) * 2) / 8)

enum algorithm {
	_ALGORITHM_INVALID = 0,
#define VMODENUM(x) x,
#include "tbl_algorithm.h"
	__MAX_ALGORITHM
};

enum scope {
	_SCOPE_INVALID = 0,
#define VMODENUM(x) x,
#include "tbl_scope.h"
	__MAX_SCOPE
};

typedef union hash_ctx {
	uint32_t	uint32[2];
	md5_ctx		md5;
	sha1_ctx	sha1;
	sha256_ctx	sha224;
	SHA256_CTX	sha256;
	sha512_ctx	sha512;
	sha3_ctx	sha3;
} hash_ctx;

static const struct hashspec {
	const size_t digestsz;
	const size_t blocksz;
} hashspec[] = {
	[CRC32] = {
		sizeof(uint32_t),
		sizeof(uint32_t),
	},
	[ICRC32] = {
		sizeof(uint32_t),
		sizeof(uint32_t),
	},
	[MD5] = {
		md5_hash_size,
		md5_block_size,
	},
	[RS] = {
		sizeof(uint32_t),
		sizeof(uint32_t),
	},
	[SHA1] = {
		sha1_hash_size,
		sha1_block_size,
	},
	[SHA224] = {
		sha224_hash_size,
		sha256_block_size,
	},
	[SHA256] = {
		SHA256_LEN,
		sha256_block_size,
	},
	[SHA384] = {
		sha384_hash_size,
		sha512_block_size,
	},
	[SHA512] = {
		sha512_hash_size,
		sha512_block_size,
	},
	[SHA3_224] = {
		sha3_224_hash_size,
		SHA3_BLOCKSZ(224),
	},
	[SHA3_256] = {
		sha3_256_hash_size,
		SHA3_BLOCKSZ(256),
	},
	[SHA3_384] = {
		sha3_384_hash_size,
		SHA3_BLOCKSZ(384),
	},
	[SHA3_512] = {
		sha3_512_hash_size,
		SHA3_BLOCKSZ(512),
	},
};
